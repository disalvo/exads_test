<?php
/**
 * Write a PHP script to generate a random array of 500 integers (values of 1 – 500 inclusive).
 * Randomly remove and discard an arbitary element from this newly generated array.
 * Write the code to efficiently determine the value of the missing element.
 * Explain your reasoning with comments.
 */
// Creating an array with 500 elements (1 to 500)
$elements = range(1, 500);
// Shuffling the numbers into array
shuffle($elements);
// Creating a copy of shuffle array
$elementsCopy = $elements;
// Generating a random number with the same range of array (1 to 500)
$randomElementNumber = mt_rand(1, 500);
// Removing one element with random index
unset($elementsCopy[$randomElementNumber]);
// Difference between two arrays (the origin and the copy without removed element), to get the missing element
$elementMissing = array_diff($elements, $elementsCopy);
// Displaying the result
echo 'Index missing: ' . key($elementMissing) . ' | Value missing: '.current($elementMissing);