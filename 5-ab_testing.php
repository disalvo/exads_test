<?php
/**
 * Exads would like to A/B test a number of promotional designs to see which provides the best conversion rate.
 * Write a snippet of PHP code that redirects end users to the different designs based on the database table below.
 * Extend the database model as needed.
 * i.e. - 50% of people will be shown Design A, 25% shown Design B and 25% shown Design C.
 * The code needs to be scalable as a single promotion may have upwards of 3 designs to test.
 * design_id; design_name; split_percent
 * 1;Design 1;50
 * 2;Design 2;25
 * 3;Design 3;25
 *
 * DDL to create table:
 *
 * CREATE TABLE `exads_ab` (
 *  `id` INT NOT NULL AUTO_INCREMENT ,
 *  `design` INT NOT NULL ,
 *  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
 *  PRIMARY KEY (`id`)
 * ) ENGINE = InnoDB;
 */

// If user clicked
if (array_key_exists('design', $_POST) && !empty($_POST['design'])) {
    require_once './dbconfig.php';
    require_once './Connection.php';

    $conn = new Connection(DB_HOST, DB_PORT, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

    if($conn->insert('exads_ab', ['design'=> $_POST['design']])) {
        unset($_POST['design']);
        header('Location: ab_results.php');
        die();
    }
}

$randomNumber = mt_rand(1, 100);

if($randomNumber <= 50) {
    $design = 1;
} elseif ($randomNumber > 50 && $randomNumber <= 75) {
    $design = 2;
} else {
    $design = 3;
}

?>

<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Exads A/B Testing</title>
    <meta name="description" content="Exads A/B Testing">
    <meta name="author" content="Alexandre Di Salvo">
    <style type="text/css">
        .box {
            height: 200px;
            width: 200px;
        }

        .box-blue {
            background-color: blue;
        }

        .box-red {
            background-color: red;
        }

        .box-green {
            background-color: green;
        }
    </style>
</head>

<body>
    <?php include("./html/{$design}.html"); ?>
</body>
</html>