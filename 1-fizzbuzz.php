<?php

/**
 * Write a PHP script that prints all integer values from 1 to 100.
 * For multiples of three output "Fizz" instead of the value and for the multiples of five output "Buzz".
 * Values which are multiples of both three and five should output as "FizzBuzz".
 *
 * @param int $number
 * @return string
 */
function fizzbuzz(int $number): string
{
    // Is multiple of three?
    $isFizz = $number % 3 === 0;
    // Is multiple of five?
    $isBuzz = $number % 5 === 0;
    // Is both cases?
    $fizzBuzz = ($isFizz ? 'Fizz' : '') . ($isBuzz ? 'Buzz' : '');

    return  (strlen($fizzBuzz) !== 0)
        ? $fizzBuzz
        : (string) $number;
}

// Range from 1 to 100
foreach (range(1, 100) as $number)
{
    echo ' ' . fizzBuzz($number) . ' ';
}