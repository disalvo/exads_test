<?php
/**
 * The Irish National Lottery draw takes place twice weekly on a Wednesday and a Saturday at 8pm.
 * Write a function or class that calculates and returns the next valid draw date based on the current date and time
 * AND also on an optionally supplied date and time.
 */

$txt = 'The next valid draw date is: ';
$date = new \DateTime();

// If user supplied the date
if(array_key_exists(1, $argv)) {
    $date = \DateTime::createFromFormat('Y-m-d H:i', $argv[1]);

    if(!$date) {
        die('Error: You need to enter a valid date [Format: Y-m-d H:i].'. PHP_EOL);
    }
}

// Verifing if the supplied date is Wednesday or Saturday
if(in_array($date->format('N'), [3, 6])) {
    // If this date is before 8pm, the next valid date is today at 8pm
    if((int) $date->format('H') < 20) {
        echo $txt . $date->format('Y-m-d 20:00') . PHP_EOL;
        die;
    }
}

// Next valids dates of draw
$nextWednesday = clone $date;
$nextSaturday = clone $date;
$nextWednesday->modify('next Wednesday 8pm');
$nextSaturday->modify('next Saturday 8pm');

echo $txt . (($nextWednesday < $nextSaturday)
    ? $nextWednesday->format('Y-m-d H:i')
    : $nextSaturday->format('Y-m-d H:i')) . PHP_EOL;