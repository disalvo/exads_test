<?php
/**
 * Demonstrate with PHP how you would connect to a MySQL (InnoDB) database and query for all
 * records with the following fields: (name, age, job_title) from a table called 'exads_test'.
 *
 * Also provide an example of how you would write a sanitised record to the same table.
 */

require_once './dbconfig.php';
require_once './Connection.php';

$conn = new Connection(DB_HOST, DB_PORT, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

$conn->insert('exads_test', ['name'=> 'Harvey Specter', 'age' => 32, 'job_title' => "Lawyer"]);
$conn->insert('exads_test', ['name'=> 'Michael Ross', 'age' => 29, 'job_title' => "Lawyer"]);
$conn->insert('exads_test', ['name'=> 'Louis Litt', 'age' => 33, 'job_title' => "Lawyer"]);

$records = $conn->select('exads_test', ['name', 'age', 'job_title']);

echo "<pre>";
print_r($records);
die;