<?php

class Connection
{
    protected $conn = null;

    function __construct($host, $port, $username = null, $password = null, $dbname = null) {
        try {
            $dsn = "mysql:host={$host};port={$port};dbname={$dbname}";

            if(is_null($this->conn)) {
                $this->conn = new PDO($dsn, $username, $password);
            }
        }
        catch(PDOException $e) {
            die('Error: ' . $e->getMessage());
        }
    }

    public function insert(string $table, array $data): bool
    {
        $values  = [];
        $valuesBind  = [];

        foreach ($data as $col => $value) {
            $valuesBind[] = $value;
            $values[]  = '?';
        }

        $sql = 'INSERT INTO '.$table.' (' . implode(', ', array_keys($data)) . ') VALUES (' . implode(', ', $values) . ')';

        $stmt = $this->conn->prepare($sql);

        return $stmt->execute($valuesBind);
    }

    public function select(string $table, array $fields = []): array
    {
        if(empty($fields)) {
            $fields[] = '*';
        }

        $sql = 'SELECT ' . implode(', ', $fields) . ' FROM ' . $table;

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function query($sql)
    {
        return $this->conn->query($sql);
    }


}