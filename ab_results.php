<?php
require_once './dbconfig.php';
require_once './Connection.php';

$conn = new Connection(DB_HOST, DB_PORT, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

$sql = 'select design, count(*) as qtt from exads_ab group by design order by design';
$rows = $conn->query($sql);

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

?>

<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Exads A/B Testing</title>
    <meta name="description" content="Exads A/B Testing">
    <meta name="author" content="Alexandre Di Salvo">
</head>

<body>
    <h1>Results</h1>

    <table>
        <thead>
            <tr>
                <th>Design</th>
                <th>Quantity</th>
            </tr>
        </thead>
        <tbody>
        <?php
            foreach ($rows as $row) { ?>
            <tr>
                <td>Design <?php echo $row['design']; ?></td>
                <td><?php echo $row['qtt']; ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <button onclick="window.location='5-ab_testing.php';">Return to Test AB</button>
</body>
</html>