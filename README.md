# ExaAds PHP Test #

## Versions:

I used in my development environment PHP 7.4 and MySQL 5.7.

### To run the script, I used PHP CLI.

1)  FizzBuzz script

    $> php 1-fizzbuzz.php

2)  500 element script

    $> php 2-500-element-array.php

3)  Database Connectivity

    I wrote a simple class to connect to the MySQL server using PDO, called Connection.php. Also, I wrote two
    functions to perform the insertion and query operation.

    For sure, has several points of improvements and adaptations to make the code more flexible.

    $> cat 3-database-connectivity.php

4)  Date Calculation

    - Using the current date time
    $> php 4-date-calculation.php

    - Using one supplied date time
    $> php 4-date-calculation.php "2020-06-10 20:59"

5)  AB Testing

    - If run the code with the PHP built-in web server.
    $> php -S 127.0.0.1:3000

    - And in your browser, access the page below:
    http://127.0.0.1:3000/5-ab_testing.php
